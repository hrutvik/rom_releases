#!/bin/bash
# Copyright (C) 2019 hsj51 <hrutvikjagtap51@gmail.com>
#
# Licensed under the Raphielscape Public License, Version 1.b (the "License");
# you may not use this file except in compliance with the License.
#

echo "***BuildBot***"
echo $TELEGRAM_TOKEN >/tmp/tg_token
echo $TELEGRAM_CHAT >/tmp/tg_chat
echo $GITHUB_TOKEN >/tmp/gh_token

cp github-release /usr/bin
cp telegram /usr/bin
bash -c "bash build.sh"
